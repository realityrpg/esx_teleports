ESX                           = nil

local PlayerData              = {}

Citizen.CreateThread(function ()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
        Citizen.Wait(1)
    end

    while ESX.GetPlayerData() == nil do
        Citizen.Wait(10)
    end

    PlayerData = ESX.GetPlayerData()

    LoadMarkers()
end) 

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
    PlayerData.job = job
end)

function LoadMarkers()
    Citizen.CreateThread(function()
    
        while true do
            Citizen.Wait(5)

            local plyCoords = GetEntityCoords(PlayerPedId())

            for location, val in pairs(Config.Teleporters) do

                local Enter = val['Enter']
                local Exit = val['Exit']
                local JobNeeded = val['Job']
				local PinCode = val['Pin']

                local dstCheckEnter, dstCheckExit = GetDistanceBetweenCoords(plyCoords, Enter['x'], Enter['y'], Enter['z'], true), GetDistanceBetweenCoords(plyCoords, Exit['x'], Exit['y'], Exit['z'], true)

                if dstCheckEnter <= 1.5 then
                    if JobNeeded ~= 'none' then
                        if PlayerData.job.name == JobNeeded then

                            DrawM(Enter['Information'], 0, Enter['x'], Enter['y'], Enter['z'])

                            if dstCheckEnter <= 1.2 then
                                if IsControlJustPressed(0, 38) then
                                    Teleport(val, 'enter')
                                end
                            end

                        end
                    else
					
                        DrawM(Enter['Information'], 0, Enter['x'], Enter['y'], Enter['z'])

                        if dstCheckEnter <= 1.2 then
                            if IsControlJustPressed(0, 38) then
                                if PinCode ~= false then
										ESX.ShowAdvancedNotification('Sicherheitssystem', 'PIN-Tastenfeld', 'Diese TÃ¼r ist durch ein PIN-Tastenfeld gesichert.~n~~y~Bitte gebe die '..string.len(PinCode)..'-stellige PIN ein.', 'CHAR_BLOCKED', 7)
										Wait(150)
										DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP", "", "", "", "", "", string.len(PinCode))
										while (UpdateOnscreenKeyboard() == 0) do
											DisableAllControlActions(0)
											Wait(0)
										end
										if (GetOnscreenKeyboardResult()) then
											local result = tostring(GetOnscreenKeyboardResult())
											if result == PinCode then
												Teleport(val, 'enter')
											else
												ESX.ShowAdvancedNotification('Sicherheitssystem', 'PIN-Tastenfeld', 'Ein Fehler ist aufgetreten:~n~~r~Die eingegebene PIN ist falsch.~n~~s~Bitte versuche es erneut.', 'CHAR_BLOCKED', 7)
											end
										end
								else
									Teleport(val, 'enter')
								end
                            end

                        end

                    end
                end

                if dstCheckExit <= 1.5 then
                    if JobNeeded ~= 'none' then
                        if PlayerData.job.name == JobNeeded then

                            DrawM(Exit['Information'], 0, Exit['x'], Exit['y'], Exit['z'])

                            if dstCheckExit <= 1.2 then
                                if IsControlJustPressed(0, 38) then
                                    Teleport(val, 'exit')
                                end
                            end

                        end
                    else

                        DrawM(Exit['Information'], 0, Exit['x'], Exit['y'], Exit['z'])

                        if dstCheckExit <= 1.2 then

                            if IsControlJustPressed(0, 38) then
                                Teleport(val, 'exit')
                            end

                        end
                    end
                end

            end

        end

    end)
end

function Teleport(table, location)
    if location == 'enter' then
        DoScreenFadeOut(300)

        Citizen.Wait(550)

        ESX.Game.Teleport(PlayerPedId(), table['Exit'])

        DoScreenFadeIn(1000)
    else
        DoScreenFadeOut(300)

        Citizen.Wait(550)

        ESX.Game.Teleport(PlayerPedId(), table['Enter'])

        DoScreenFadeIn(1000)
    end
end


function DrawM(hint, type, x, y, z)
	ESX.ShowHelpNotification(hint)
	DrawMarker(type, x, y, z+0.5, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 0.75, 0.75, 0.75, 255, 0, 0, 75, false, true, 2, false, false, false, false)
end
