Config                            = {}

Config.Teleporters = {
	['MafiaBase'] = {
		['Job'] = 'none',
		['Pin'] = '131691',
		['Enter'] = { 
			['x'] = -613.03, 
			['y'] = -1624.33, 
			['z'] = 32.50,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um den Aufzug zu benutzen',
		},
		['Exit'] = {
			['x'] = 2155.44, 
			['y'] = 2920.92, 
			['z'] = -61.80,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um den Aufzug zu benutzen' 
		}
	},
	['MafiaBaseBoss'] = {
		['Job'] = 'mafia',
		['Pin'] = false,
		['Enter'] = { 
			['x'] = 2128.5, 
			['y'] = 2925.49, 
			['z'] = -61.9, 
			['Information'] = 'Drücke ~INPUT_PICKUP~ um die Tür zu benutzen',
		},
		['Exit'] = {
			['x'] = 2122.42, 
			['y'] = 2927.66, 
			['z'] = -61.9,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um die Tür zu benutzen' 
		}
	},
	['MafiaBaseServerRoom'] = {
		['Job'] = 'mafia',
		['Pin'] = false,
		['Enter'] = { 
			['x'] = 2033.85, 
			['y'] = 2942.12, 
			['z'] = -61.91, 
			['Information'] = 'Drücke ~INPUT_PICKUP~ um den Aufzug zu benutzen',
		},
		['Exit'] = {
			['x'] = 2154.68, 
			['y'] = 2921.04, 
			['z'] = -81.08,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um den Aufzug zu benutzen' 
		}
	},
	['moneywash_int'] = {
		['Job'] = 'none',
		['Pin'] = '99857',
		['Enter'] = { 
			['x'] = 1197.19, 
			['y'] = -3253.61, 
			['z'] = 6.5,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um das Gebäude zu betreten',
		},
		['Exit'] = {
			['x'] = 1138.1, 
			['y'] = -3199.1, 
			['z'] = -40.00,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um das Gebäude zu verlassen' 
		}
	},
	['comedyclub_int'] = {
		['Job'] = 'none',
		['Pin'] = false,
		['Enter'] = { 
			['x'] = -430.13, 
			['y'] = 261.87, 
			['z'] = 83.0,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um das Gebäude zu betreten',
		},
		['Exit'] = {
			['x'] = -459.24, 
			['y'] = 284.84, 
			['z'] = 78.50,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um das Gebäude zu verlassen' 
		}
	},
	['police_intel'] = {
		['Job'] = 'none',
		['Pin'] = false,
		['Enter'] = { 
			['x'] = 446.42, 
			['y'] = -986.53, 
			['z'] = 26.67,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um den Verhörraum zu betreten',
		},
		['Exit'] = {
			['x'] = 442.12, 
			['y'] = -996.99, 
			['z'] = 4.80,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um die Tür zu benutzen' 
		}
	},
	['police_office'] = {
		['Job'] = 'police',
		['Pin'] = false,
		['Enter'] = { 
			['x'] = 452.03, 
			['y'] = -984.15, 
			['z'] = 26.67,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um das Gemeinschaftsbüro zu betreten',
		},
		['Exit'] = {
			['x'] = 442.06, 
			['y'] = -986.87, 
			['z'] = 4.80,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um die Tür zu benutzen' 
		}
	},
	['police_storage'] = {
		['Job'] = 'police',
		['Pin'] = false,
		['Enter'] = { 
			['x'] = 470.8, 
			['y'] = -984.64, 
			['z'] = 30.69,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um die Asservatenkammer zu betreten',
		},
		['Exit'] = {
			['x'] = 456.24, 
			['y'] = -1000.88, 
			['z'] = 4.80,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um die Tür zu benutzen' 
		}
	},
	['bahamas'] = {
		['Job'] = 'none',
		['Pin'] = false,
		['Enter'] = {
			['x'] = -1388.89,
			['y'] = -585.91,
			['z'] = 29.85,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um das Bahama Mamas zu betreten',
		},
		['Exit'] = {
			['x'] = -1387.9, 
			['y'] = -587.52, 
			['z'] = 29.85,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um das Bahama Mamas zu verlassen' 
		}
	},

	-- DRUGS --
	['drugs_transform_coke'] = {
		['Job'] = 'none',
		['Pin'] = false,
		['Enter'] = {
			['x'] = -1568.83,
			['y'] = -3224.41,
			['z'] = 26.00,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um die Tür zu verwenden'
		},
		['Exit'] = {
			['x'] = 1088.62,
			['y'] = -3187.5,
			['z'] = -38.99,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um die Tür zu verwenden'
		},
	},
	['drugs_farm_transform_weed'] = {
		['Job'] = 'none',
		['Pin'] = false,
		['Enter'] = {
			['x'] = -1146.61,
			['y'] = 4940.91,
			['z'] = 222.27,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um die Tür zu verwenden'
		},
		['Exit'] = {
			['x'] = 1066.3,
			['y'] = -3183.39,
			['z'] = -39.16,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um die Tür zu verwenden'
		},
	},
	['drugs_transform_meth'] = {
		['Job'] = 'none',
		['Pin'] = false,
		['Enter'] = { 
			['x'] = -957.00, 
			['y'] = -225.13, 
			['z'] = 19.75,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um die Tür zu verwenden',
		},
		['Exit'] = {
			['x'] = 997.16, 
			['y'] = -3200.84, 
			['z'] = -36.40,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um die Tür zu verwenden' 
		}
	},
	['fib_tower'] = {
		['Job'] = 'none',
		['Pin'] = false,
		['Enter'] = { 
			['x'] = 136.17, 
			['y'] = -761.83, 
			['z'] = 45.00,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um den Aufzug zu benutzen',
		},
		['Exit'] = {
			['x'] = 136.17, 
			['y'] = -761.83, 
			['z'] = 242.15,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um den Aufzug zu benutzen' 
		}
	},
	['casino_int'] = {
		['Job'] = 'none',
		['Pin'] = false,
		['Enter'] = { 
			['x'] = 887.37, 
			['y'] = -953.78, 
			['z'] = 39.21,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um die Tür zu benutzen',
		},
		['Exit'] = {
			['x'] = 906.48, 
			['y'] = -942.97, 
			['z'] = 44.00,
			['Information'] = 'Drücke ~INPUT_PICKUP~ um die Tür zu benutzen' 
		}
	},
}